package ant

import (
	"gitlab.com/SiaPrime/SiaPrime/node/api/client"
	"gitlab.com/SiaPrime/SiaPrime/sync"
)

// A jobRunner is used to start up jobs on the running Sia node.
type jobRunner struct {
	client            *client.Client
	walletPassword    string
	siaprimeDirectory string
	tg                sync.ThreadGroup
	hostAddr          string
}

// newJobRunner creates a new job runner, using the provided api address,
// authentication password, and sia directory.  It expects the connected api to
// be newly initialized, and initializes a new wallet, for usage in the jobs.
// siaprimedirectory is used in logging to identify the job runner.
func newJobRunner(apiaddr, hostaddr string, authpassword string, siaprimedirectory string) (*jobRunner, error) {
	client := client.New(apiaddr)
	client.Password = authpassword
	jr := &jobRunner{
		client:            client,
		siaprimeDirectory: siaprimedirectory,
		hostAddr:          hostaddr,
	}
	walletParams, err := jr.client.WalletInitPost("", false)
	if err != nil {
		return nil, err
	}
	jr.walletPassword = walletParams.PrimarySeed

	err = jr.client.WalletUnlockPost(jr.walletPassword)
	if err != nil {
		return nil, err
	}

	return jr, nil
}

// Stop signals all running jobs to stop and blocks until the jobs have
// finished stopping.
func (j *jobRunner) Stop() {
	j.tg.Stop()
}
