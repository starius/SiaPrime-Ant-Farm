/*
Package ant provides an abstraction for the functionality of 'ants' in the
antfarm. Ants are Sia clients that have a myriad of user stories programmed as
their behavior and report their successfullness at each user store.
*/
package ant

import (
	"errors"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"time"

	"gitlab.com/SiaPrime/SiaPrime/node/api/client"
)

// newSpd spawns a new spd process using os/exec and waits for the api to
// become available.  spdPath is the path to Spd, passed directly to
// exec.Command.  An error is returned if starting spd fails, otherwise a
// pointer to spd's os.Cmd object is returned.  The data directory `datadir`
// is passed as spd's `--siaprime-directory`.
func newSpd(spdPath string, datadir string, apiAddr string, rpcAddr string, hostAddr string, apiPassword string) (*exec.Cmd, error) {
	if err := checkSpdConstants(spdPath); err != nil {
		return nil, err
	}
	// create a logfile for Sia's stderr and stdout.
	logfile, err := os.Create(filepath.Join(datadir, "sia-output.log"))
	if err != nil {
		return nil, err
	}
	args := []string{"--modules=cgthmrw", "--no-bootstrap", "--siaprime-directory=" + datadir, "--api-addr=" + apiAddr, "--rpc-addr=" + rpcAddr, "--host-addr=" + hostAddr}
	if apiPassword == "" {
		args = append(args, "--authenticate-api=false")
	}
	cmd := exec.Command(spdPath, args...)
	cmd.Stderr = logfile
	cmd.Stdout = logfile
	if apiPassword != "" {
		cmd.Env = append(os.Environ(), "SIA_API_PASSWORD="+apiPassword)
	}

	if err := cmd.Start(); err != nil {
		return nil, err
	}

	if err := waitForAPI(apiAddr, cmd); err != nil {
		return nil, err
	}

	return cmd, nil
}

// checkSpdConstants runs `spd version` and verifies that the supplied spd
// is running the correct, dev, constants. Returns an error if the correct
// constants are not running, otherwise returns nil.
func checkSpdConstants(spdPath string) error {
	cmd := exec.Command(spdPath, "version")
	output, err := cmd.Output()
	if err != nil {
		return err
	}

	if !strings.Contains(string(output), "-dev") {
		return errors.New("supplied spd is not running required dev constants")
	}

	return nil
}

// stopSpd tries to stop the spd running at `apiAddr`, issuing a kill to its
// `process` after a timeout.
func stopSpd(apiAddr string, process *os.Process) {
	if err := client.New(apiAddr).DaemonStopGet(); err != nil {
		process.Kill()
	}

	// wait for 120 seconds for spd to terminate, then issue a kill signal.
	done := make(chan error)
	go func() {
		_, err := process.Wait()
		done <- err
	}()
	select {
	case <-done:
	case <-time.After(120 * time.Second):
		process.Kill()
	}
}

// waitForAPI blocks until the Sia API at apiAddr becomes available.
// if spd returns while waiting for the api, return an error.
func waitForAPI(apiAddr string, spd *exec.Cmd) error {
	c := client.New(apiAddr)

	exitchan := make(chan error)
	go func() {
		_, err := spd.Process.Wait()
		exitchan <- err
	}()

	// Wait for the Sia API to become available.
	success := false
	for start := time.Now(); time.Since(start) < 5*time.Minute; time.Sleep(time.Millisecond * 100) {
		if success {
			break
		}
		select {
		case err := <-exitchan:
			return fmt.Errorf("spd exited unexpectedly while waiting for api, exited with error: %v", err)
		default:
			if _, err := c.ConsensusGet(); err == nil {
				success = true
			}
		}
	}
	if !success {
		stopSpd(apiAddr, spd.Process)
		return errors.New("timeout: couldnt reach api after 5 minutes")
	}
	return nil
}
